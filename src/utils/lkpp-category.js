export function getLkppCategory() {
    return [
        {
            category_id: 6,
            name:'categoryFood',
            title: 'Makanan & Minuman',
            category: 'makanan',
        },
        {
            category_id: 5,
            name:'categoryFurniture',
            title: 'Furniture',
            category: 'furniture',
        },
        {
            category_id: 2,
            name:'categoryATK',
            title: 'Alat Tulis Kantor',
            category: 'alat-tulis-kantor',
        },
        {
            category_id: 9,
            name:'categorySuvenir',
            title: 'Souvenir',
            category: 'souvenir',
        },
        {
            category_id: 4,
            name:'categoryFashion',
            title: 'Fashion',
            category: 'fashion',
        },
        {
            category_id: 7,
            name:'categoryPerkakas',
            title: 'Perkakas',
            category: 'perkakas',
        },
        {
            category_id: 1,
            name:'categoryAlkes',
            title: 'Alat Kesehatan',
            category: 'alat-kesehatan',
        },
        {
            category_id: 3,
            name:'categoryElektronik',
            title: 'Elektronik',
            category: 'elektronik',
        },
        {
            category_id: 11,
            name:'categorySewaAlat',
            title: 'Sewa Peralatan & Ruangan',
            category: 'sewa-peralatan-ruangan',
        },
        {
            category_id: 14,
            name:'categoryTransportasi',
            title: 'Jasa Transportasi',
            category: 'jasa-transportasi',
        },
        {
            category_id: 13,
            name:'categoryAkomodasi',
            title: 'Akomodasi',
            category: 'akomodasi',
        },
        {
            category_id: 10,
            name:'categoryJasaKreatif',
            title: 'Jasa Kreatif Kebutuhan Kantor',
            category: 'jasa-kreatif-kebutuhan-kantor',
        },
        {
            category_id: 8,
            name:'categoryPertanianPeternakan',
            title: 'Pertanian & Peternakan',
            category: 'pertanian-peternakan',
        },
    ];
}
