import { ref } from 'vue'
import axios from 'axios';
import { useLoading } from 'vue-loading-overlay'

async function getDetailPesanan(id) {
    const pesanan = ref(null)
    const pesananError = ref(null)

    const $loading = useLoading()
    const loader = $loading.show();

    await axios
        .get(`/api/v1/pesanan/${id}`, { params: { image_base64: true } })
        .then((json) => pesanan.value = json.data.data)
        .catch((error) => {
            if(error.response.status == 404) {
                location.href = '/not-found';
            }

            pesananError.value = error
        })
        .then(() => loader.hide())

    return {
        pesanan,
        pesananError
    }
};

async function getKomplainPesanan(id) {
    const pesananKomplain = ref(null)
    const pesananKomplainError = ref(null)

    await axios
        .get(`/api/v1/pesanan/get-komplain/${id}`)
        .then((json) => pesananKomplain.value = json.data.data)
        .catch((error) => pesananKomplainError.value = error)

    return {
        pesananKomplain,
        pesananKomplainError
    }
};

export {
    getDetailPesanan,
    getKomplainPesanan
}
